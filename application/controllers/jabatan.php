<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Jabatan extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("jabatan_model");
		
		// cek login akses
		$user_login = $this->session->userdata();
		if (count($user_login) <= 1) {
			
			redirect("auth/index", "refresh");
	}
	}
	
	public function index()
	
	{
		$this->listJabatan();
	}
	
	public function listJabatan()
	
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content'] = 'forms/list_jabatan';
		$this->load->view('home', $data);
	}
	public function inputjabatan()
	
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		//if (!empty($_REQUEST)) {
			//$m_jabatan = $this->jabatan_model;
			//$m_jabatan->save();
			//redirect("jabatan/index", "refresh");
		//}
		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());
		
		if ($validation->run()) {
			$this->jabatan_model->save();
			$this->session->set_flashdata('info', '<div style="color : green">simpan data berhasil !</div>');
			redirect("jabatan/index", "refresh");
		}
			
		//$this->load->view('input_jabatan');
		$data['content'] = 'forms/input_jabatan';
		$this->load->view('home', $data);
	}
	public function detailjabatan($kode_jabatan)
	
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$data['content'] = 'forms/detail_jabatan';
		$this->load->view('home', $data);
		
	}
	public function editjabatan($kode_jabatan)
	
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		
		//if (!empty($_REQUEST)) {
			//$m_jabatan = $this->jabatan_model;
			//$m_jabatan->update($kode_jabatan);
			//redirect("jabatan/index", "refresh");
		//}
		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());
		
		if ($validation->run()) {
			$this->jabatan_model->update($kode_jabatan);
			$this->session->set_flashdata('info', '<div style="color : green">UPDATE data berhasil !</div>');
			redirect("jabatan/index", "refresh");
			
		}
		//$this->load->view('edit_jabatan', $data);
		$data['content'] = 'forms/edit_jabatan';
		$this->load->view('home', $data);
	}
	public function deletejabatan($kode_jabatan)
	
	{
			$m_jabatan = $this->jabatan_model;
			$m_jabatan->delete($kode_jabatan);
			redirect("jabatan/index", "refresh");
	}
}
